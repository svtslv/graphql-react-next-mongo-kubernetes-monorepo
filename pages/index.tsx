import * as React from 'react';
import Head from 'next/head';
import Layout from '../components/Layout'

export default function Home() {
  return (
    <Layout>
      <div className="container">
        <Head>
          <title>Create Next App</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <div>Index Page</div>
      </div>
    </Layout>
  )
}
