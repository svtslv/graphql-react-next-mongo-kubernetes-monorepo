import { ApolloServer, gql } from "apollo-server-micro";
import { typeDefs } from '../../src/typeDefs';
import { resolvers } from '../../src/resolvers';

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  context: (context) => context
});

const handler = apolloServer.createHandler({ path: "/api/graphql" });

export const config = {
  api: {
    bodyParser: false
  }
};

export default handler;
