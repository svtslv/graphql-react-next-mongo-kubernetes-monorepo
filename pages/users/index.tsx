import * as React from 'react';
import Link from 'next/link';
import { gql } from 'apollo-boost';
import Layout from '../../components/Layout';
import { Query, Mutation } from 'react-apollo';
import { Button, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { useState } from 'react';

const usersQuery = gql`
    query users($skip: Int, $limit: Int) {
        users(skip: $skip, limit: $limit) {
            _id
            name
            email
        }
    }
`;

const deleteUserMutation = gql`
    mutation deleteUser($id: ID!) {
        deleteUser(id: $id) {
            _id
            name
            email
        }
    }
`;

export default function UserIndex({ initialUsers }) {

  const [users, setUsers] = useState(initialUsers);

  return (<>
    <Layout>
      <Query query={usersQuery} variables={{ skip: 0, limit: 25 }} onCompleted={result => setUsers(result.users)} fetchPolicy='network-only'>
        {result => {
          const { data, loading, error } = result;
          if (loading) {
            return <span>Loading...</span>
          }
          if (error) {
            return <span>Error...</span>
          }
          if (users) {
            return (<>
              <TableContainer component={Paper}>
                <Table aria-label="users">
                  <TableHead>
                    <TableRow>
                      <TableCell>_ID</TableCell>
                      <TableCell align="right">Name</TableCell>
                      <TableCell align="right">Email</TableCell>
                      <TableCell align="right">Actions</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {users.map((row) => (
                      <TableRow key={row._id}>
                        <TableCell component="th" scope="row">{row._id}</TableCell>
                        <TableCell align="right">{row.name}</TableCell>
                        <TableCell align="right">{row.email}</TableCell>
                        <TableCell align="right">
                          <Link href={`/users/[id]`} as={`/users/${row._id}`}><Button>View</Button></Link>
                          <Link href={`/users/[id]/edit`} as={`/users/${row._id}/edit`}><Button>Edit</Button></Link>
                          <Mutation mutation={deleteUserMutation}>
                            { (sendMutation, result) => {
                              const { mutationResult, loading, error } = result;
                              if (loading) {
                                return <span>Loading...</span>
                              }
                              if (error) {
                                return <span>Error...</span>
                              }
                              return <Button onClick={() => sendMutation({ variables: { id: row._id } }).then((result => {
                                setUsers(users => users.filter(user => user._id !== row._id));
                              }))}>Delete</Button>
                            }}
                          </Mutation>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </>);
          }
          return <span>&nbsp;</span>
        }}
      </Query>
    </Layout>
  </>)
}

UserIndex.getInitialProps = async () => {
  return { initialUsers: [] }
}
