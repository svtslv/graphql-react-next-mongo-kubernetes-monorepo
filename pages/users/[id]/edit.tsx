import * as React from 'react';
import Layout from '../../../components/Layout';
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  FormControl,
  Input,
  InputLabel
} from '@material-ui/core';
import { gql } from 'apollo-boost';
import { Mutation, Query } from 'react-apollo';
import { useState } from 'react';
import { useRouter } from 'next/router'

const userQuery = gql`
    query user($id: ID!) {
        user(id: $id) {
            _id
            name
            email
        }
    }
`

const updateUserMutation = gql`
    mutation updateUser($id: ID!, $name: String!, $email: Email!) {
        updateUser(id: $id, input: { name: $name, email: $email }) {
            _id
            name
            email
        }
    }
`

export default function UserUpdate() {
  const router = useRouter();
  const { id } = router.query;

  const initialInput = {
    id,
    name: '',
    email: ''
  }

  const [input, setInput] = useState(initialInput);

  return (<>
    <Layout>
      {id && <>
        <Query query={userQuery} variables={initialInput} onCompleted={data => setInput({ id: data.user._id, name: data.user.name, email: data.user.email })}>
          {(result) => {
            const { data, loading, error } = result;
            if (loading) {
              return <span>Loading...</span>
            }
            if (error) {
              return <span>Error...</span>
            }
            if (data) {
              return (<>
                <Mutation mutation={updateUserMutation}>
                  {(sendMutation, result) => {
                    const {data, loading, error} = result;
                    if (loading) {
                      return <span>Loading...</span>
                    }
                    if (error) {
                      return <span>Error...</span>
                    }
                    if (data) {
                      const id = result?.data?.updateUser?._id;
                      console.log(result);
                      if(id) {
                        router.push(`/users/[id]`, `/users/${id}`).then();
                        return <span>Redirect...</span>
                      }
                    }

                    return (<>
                      <Card>
                        <CardHeader title="Create User"/>
                        <CardContent>
                          <FormControl fullWidth>
                            <InputLabel htmlFor="name">Name</InputLabel>
                            <Input id="name" aria-describedby="Name" value={input.name} onChange={event => setInput({ ...input, name: event.target.value })} />
                          </FormControl>
                          <FormControl fullWidth>
                            <InputLabel htmlFor="email">Email</InputLabel>
                            <Input id="email" aria-describedby="Email" value={input.email} onChange={event => setInput({ ...input, email: event.target.value })}/>
                          </FormControl>
                        </CardContent>
                        <CardActions disableSpacing>
                          <Button style={{marginLeft: 'auto'}} onClick={() => {
                            console.log(input);
                            sendMutation({ variables: { id: input.id, name: input.name, email: input.email } }).then();
                          }}>Save</Button>
                        </CardActions>
                      </Card>
                    </>)
                  }}
                </Mutation>
              </>)
            }
          }}
        </Query>
      </>}
    </Layout>
  </>)
}
