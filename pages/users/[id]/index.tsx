import * as React from 'react';
import Layout from '../../../components/Layout';
import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';
import { useRouter } from 'next/router';

const userQuery = gql`
    query user($id: ID!) {
        user(id: $id) {
            _id
            name
            email
        }
    }
`;

export default function UserShow() {
  const router = useRouter();
  const { id } = router.query;

  return (<>
    <Layout>
      {id && <>
        <Query query={userQuery} variables={{ id: id }}>
          {(result) => {
            const { data, loading, error } = result;
            if (loading) {
              return <span>Loading...</span>
            }
            if (error) {
              return <span>Error...</span>
            }
            if (data) {
              return (<div>
                <div>ID: {data.user._id}</div>
                <div>Name: {data.user.name}</div>
                <div>Email: {data.user.email}</div>
              </div>)
            }
          }}
        </Query>
      </>}
    </Layout>
  </>)
}
