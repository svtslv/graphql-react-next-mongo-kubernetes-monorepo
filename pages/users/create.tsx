import * as React from 'react';
import Layout from '../../components/Layout';
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  FormControl,
  Input,
  InputLabel
} from '@material-ui/core';
import { gql } from 'apollo-boost';
import { Mutation } from 'react-apollo';
import { useState } from 'react';
import { useRouter } from 'next/router'

const createUserMutation = gql`
    mutation createUser($name: String!, $email: Email!) {
        createUser(input: { name: $name, email: $email }) {
            _id
            name
            email
        }
    }
`

export default function UserCreate() {
  const router = useRouter()

  const initialInput = {
    name: '',
    email: ''
  }

  const [input, setInput] = useState(initialInput);

  return (<>
    <Layout>
      <Mutation mutation={createUserMutation}>
        {(sendMutation, result) => {
          const {data, loading, error} = result;
          if (loading) {
            return <span>Loading...</span>
          }
          if (error) {
            return <span>Error...</span>
          }
          if (data) {
            const id = result?.data?.createUser?._id;
            if(id) {
              router.push(`/users/[id]`, `/users/${id}`).then();
              return <span>Redirect...</span>
            }
          }

          return (<>
            <Card>
              <CardHeader title="Create User"/>
              <CardContent>
                <FormControl fullWidth>
                  <InputLabel htmlFor="name">Name</InputLabel>
                  <Input id="name" aria-describedby="Name" value={input.name} onChange={event => setInput({ ...input, name: event.target.value })} />
                </FormControl>
                <FormControl fullWidth>
                  <InputLabel htmlFor="email">Email</InputLabel>
                  <Input id="email" aria-describedby="Email" value={input.email} onChange={event => setInput({ ...input, email: event.target.value })}/>
                </FormControl>
              </CardContent>
              <CardActions disableSpacing>
                <Button style={{marginLeft: 'auto'}} onClick={() => {
                  sendMutation({ variables: { name: input.name, email: input.email } }).then();
                  setInput(initialInput);
                }}>Save</Button>
              </CardActions>
            </Card>
          </>)
        }}
      </Mutation>
    </Layout>
  </>)
}
