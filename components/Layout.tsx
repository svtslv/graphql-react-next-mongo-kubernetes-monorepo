import * as React from 'react'
import { AppBar, Container, Typography, Button, Toolbar } from '@material-ui/core';
import Link from 'next/link'
import { ApolloProvider } from 'react-apollo';
import { apolloClient } from '../src/apolloClient';

function Layout({ children }) {
  return (<>
    <AppBar position="static">
      <Container maxWidth="md">
        <Toolbar style={{ padding: 0 }}>
          <Typography variant="h6" style={{ marginTop: '-2px', marginRight: '10px' }}>GraphQL</Typography>
          {/*<Link href="/"><Button color="inherit">Main</Button></Link>*/}
          <Link href="/users"><Button color="inherit">Users</Button></Link>
          <Link href="/users/create"><Button color="inherit">Add user</Button></Link>
        </Toolbar>
      </Container>
    </AppBar>
    <Container maxWidth="md" style={{ padding: '16px' }}>
      <ApolloProvider client={ apolloClient }>
        {children}
      </ApolloProvider>
    </Container>
  </>)
}

export default Layout
