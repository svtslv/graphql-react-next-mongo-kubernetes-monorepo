FROM node:12-alpine

WORKDIR /app
EXPOSE 4200

COPY package* ./
RUN apk update && apk add --no-cache --virtual .build-deps make gcc g++ python \
 && npm ci \
 && apk del .build-deps
COPY . .

CMD ["npm", "run", "dev"]
