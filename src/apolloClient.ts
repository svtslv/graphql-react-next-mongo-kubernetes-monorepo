import ApolloClient from 'apollo-boost';
import fetch from 'node-fetch';

export const apolloClient = new ApolloClient({
  uri: 'http://localhost:3000/api/graphql',
  fetch: fetch as any
});
