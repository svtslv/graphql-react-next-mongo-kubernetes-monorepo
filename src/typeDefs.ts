import { GraphQLScalarType } from 'graphql';
import { gql } from 'apollo-server-micro';

export const Email = new GraphQLScalarType({
  name: "Email",
  description: "Email Scalar",
  serialize: value => value
});

export const typeDefs = gql`
    scalar Email

    type User {
        _id: ID!
        email: Email!
        name: String!
    }

    input CreateUserInput {
        email: Email!
        name: String!
    }

    input UpdateUserInput {
        email: Email
        name: String
    }

    type Query {
        user(id: ID!): User!
        users(skip: Int = 0, limit: Int = 10): [User]
    }

    type Mutation {
        createUser(input: CreateUserInput!): User!
        updateUser(id: ID!, input: UpdateUserInput!): User!
        deleteUser(id: ID!): User!
    }
`;
