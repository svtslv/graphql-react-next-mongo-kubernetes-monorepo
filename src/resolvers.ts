import { IUser, UserModel } from './mongoose';

export const resolvers = {
  Query: {
    user: async (_parent, { id }, _context): Promise<IUser> => {
      const user = await UserModel.findById(id);
      return user;
    },

    users: async (_parent, { skip, limit  }, _context): Promise<IUser[]> => {
      const users = await UserModel.find().skip(skip).limit(limit);
      return users || [];
    }
  },

  Mutation: {
    createUser: async (_parent, { input: { name, email } }, _context) => {
      const user = new UserModel();
      user.name = name;
      user.email = email;
      return await user.save();
    },

    updateUser: async (_parent, { id, input: { name, email } }, _context): Promise<IUser> => {
      const user = await UserModel.findById(id);
      user.name = name;
      user.email = email;
      return await user.save();
    },

    deleteUser: async (_parent, { id }, _context): Promise<IUser> => {
      const user = await UserModel.findById(id);
      return await user.remove();
    },
  }
};
