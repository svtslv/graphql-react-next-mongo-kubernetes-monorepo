import * as mongoose from 'mongoose';

mongoose.connect(process.env.MONGO_BD_1_URL || 'mongodb://USER:PASS@localhost/MONGO_DATABASE', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
}).then();

export interface IUser extends mongoose.Document {
  _id: string,
  name: string,
  email: string,
}

export const UserSchema = new mongoose.Schema({
  name: String,
  email: String,
});

// Quick Fix (MongooseError: Cannot overwrite User model once compiled.)
global['UserSchema'] = global['UserSchema'] || mongoose.model<IUser>('User', UserSchema);
export const UserModel = global['UserSchema'] as mongoose.Model<IUser>;
